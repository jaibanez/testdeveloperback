/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prueba.demo.controller;

import com.prueba.demo.entity.Employe;
import com.prueba.demo.service.EmployeService;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 *
 * @author User
 */
@Controller
public class EmployeController {
    
    @Autowired
    private EmployeService employeService;
    
    //Create
    public Employe save(Employe employe){
        return this.employeService.save(employe);
    }
    
    //Update
    public Employe update(Employe employe){
        employe.setUpdatedAt(Date.from(Instant.now()));
        return this.employeService.save(employe);
    }
    
    //Delete
    public Boolean delete(Integer id){
        return this.employeService.delete(id);
    }
    
    //List
    public List<Employe> findAll(){
        return this.employeService.findAll();
    }
    
    //Employe By Email
    public Employe findOneByEmail(String email){
        return this.employeService.findOneByEmail(email);
    }
    
    //Employe By Id
    public Employe finOne(Integer id){
        return this.employeService.findOne(id);
    }
}
