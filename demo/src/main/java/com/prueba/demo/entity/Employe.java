/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prueba.demo.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.time.Instant;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;

/**
 *
 * @author User
 */
@Entity
@Table(name="employer")
public class Employe implements Serializable {
    
    public Employe() {
        this.createdAt = Date.from(Instant.now());
        this.deletedAt = false;
        this.updatedAt = Date.from(Instant.now());
    }
    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)    
    private Integer id;
    
    @Column(name = "fullname")
    private String fullname;
    
    @Column(name = "function")
    private String function;
    
    @Column(name = "email")
    private String email;
    
    @Column(name = "phone")
    private String phone;
    
    @Lob
    @Column(name = "boss")
    private Employe boss;
    
    @Column(name ="created_at")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date createdAt;
    
    @Column(name="updated_at")
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date updatedAt;
    
    @Column(name="deleted_at")
    private Boolean deletedAt;
    
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getFunction() {
        return function;
    }

    public void setFunction(String function) {
        this.function = function;
    }

    public Employe getBoss() {
        return boss;
    }

    public void setBoss(Employe boss) {
        this.boss = boss;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Boolean getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(Boolean deletedAt) {
        this.deletedAt = deletedAt;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
    
    
}
