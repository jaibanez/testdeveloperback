/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prueba.demo.service;

import com.prueba.demo.entity.Employe;
import com.prueba.demo.repository.EmployeRepository;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author User
 */
@Service
public class EmployeService {
    
    @Autowired
    private EmployeRepository employeRepository;
   
    public List<Employe> findAll(){
        return this.employeRepository.findAll();
    }
    
    public Employe findOne(Integer id){
        return  this.employeRepository.getOne(id);
    }
    
    public Employe save(Employe employe){
        return this.employeRepository.save(employe);
    }
    
    public Boolean delete(Integer id){
        if(this.employeRepository.findById(id).isPresent()){
            employeRepository.deleteById(id);
            return true;
        }
        return false;
    }
    
    public Employe findOneByEmail(String email){
        return this.employeRepository.findOneByEmail(email);
    }
    
}
