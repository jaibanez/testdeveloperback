package com.prueba.demo.api;


import com.prueba.demo.controller.EmployeController;
import com.prueba.demo.entity.Employe;
import com.prueba.demo.entity.Message;
import io.swagger.annotations.ApiOperation;
import io.swagger.models.Response;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author User
 */
@RestController
@RequestMapping("employe")
public class EmployeApi {
    
    @Autowired
    private EmployeController employeController;
    
    @RequestMapping(value="/",method = RequestMethod.POST)
    @ApiOperation(value = "Crea un empleado en el sistema", notes = "Crear empleado", produces = "application/json" )
    public ResponseEntity<?> save(@RequestBody Employe employe){
        
        Employe employeExist = this.employeController.findOneByEmail(employe.getEmail());
        
        if(employeExist != null){
            Message m = new Message();
            m.setMessage("Este usuario ya existe!");
            return new ResponseEntity<>(m,HttpStatus.BAD_REQUEST);
        }
        
        employe = this.employeController.save(employe);
        
        return new ResponseEntity<>(employe,HttpStatus.OK);
    }
    
    @RequestMapping(value="/",method = RequestMethod.GET)
    @ApiOperation(value = "Lista todos los empleados del sistema", notes = "lista empleados", produces = "application/json" )
    public ResponseEntity<?> list(){
        
        List<Employe> list  = this.employeController.findAll();
        
        return new ResponseEntity<>(list,HttpStatus.OK);
    }
    
    @RequestMapping(value="/",method = RequestMethod.PUT)
    @ApiOperation(value = "actualiza el empleado", notes = "actualiza empleado", produces = "application/json" )
    public ResponseEntity<?> update(@RequestBody Employe employe){
        
        employe = this.employeController.update(employe);
        
        return new ResponseEntity<>(employe,HttpStatus.OK);
    }
    
}
