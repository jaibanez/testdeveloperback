/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prueba.demo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author gohan
 */
@Configuration
@EnableSwagger2
public class SpringFoxConfig{ 
    
    private static final Contact DEFAULT_CONTACT = new Contact(
            "developer",
            "www.desarrollocibernetico.com",
            "ingenieria@desarrollocibernetico.com"
    );
    private static final ApiInfo DEFAULT_API_INFO = new ApiInfo(
            "Api para administracion taller",
            "Api que gestiona ws de taller",
            "0.1",
            "urn:tos",
            "",
            "",
            "www.desarrollocibernetico.com"
    );
    private static final Set<String> DEFAULT_PRODUCES_AND_CONSUMES =
            new HashSet<String>(Arrays.asList("application/json"));

    @Bean
    public Docket api(){
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(DEFAULT_API_INFO)
                .produces(DEFAULT_PRODUCES_AND_CONSUMES)
                .consumes(DEFAULT_PRODUCES_AND_CONSUMES);
    }

}