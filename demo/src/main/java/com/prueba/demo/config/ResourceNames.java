/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prueba.demo.config;

/**
 *
 * @author USUARIO
 */
public class ResourceNames {
    
    public static final String PROPERTIES = "classpath:META-INF/application.properties";

    public static final String REST_API = "com.prueba.demo.api";

    public static final String CONTROLLERS = "com.prueba.demo.controller";

    public static final String SERVICES = "com.prueba.demo.service";
    
}
